#!/bin/bash

echo "PCFastUpdater V0.1"

#to set to development branch create a file called Development in this directory.
DIRNAME=`dirname $0`
if [ -f "$DIRNAME/Development" ];then
	BRANCH="Development"
else
	BRANCH="master"
fi

#PCFastRelease Upgrade

#to set to hardware branch create a file called RasPi or NanoPi in this directory.
if [ -f "$DIRNAME/NanoPi" ];then
	echo "NanoPi File detected."
	BACKUPUPGRADE="https://gitlab.com/JonW/PCFastRelease-NanoPi/raw/master/upgrade.sh"
	BACKUPINSTALL="https://gitlab.com/JonW/PCFastRelease-NanoPi/raw/master/install.sh"
else
	echo "No Hardware File detected. Default to RasPi"
	BACKUPUPGRADE="https://gitlab.com/JonW/PCFastRelease/raw/master/upgrade.sh"
	BACKUPINSTALL="https://gitlab.com/JonW/PCFastRelease/raw/master/install.sh"
fi

APP="PCFastRelease"
DIRECTORY="/usr/local/bin/$APP"
UPGRADEFILE="$DIRECTORY/upgrade.sh"

if [ -d "$DIRECTORY" ]; then
	echo "$DIRECTORY exists"
	
	if [ -f $UPGRADEFILE ];then
		echo "$UPGRADEFILE exists"
		echo "Run Upgrade for $APP"
		bash $UPGRADEFILE $BRANCH
	else
		echo "the upgrade file for $APP was missing."
		echo "as a backup I will run from gitlab"
		wget -O - $BACKUPUPGRADE | bash -s $BRANCH
	fi
else
	echo "The directory does not exist. This is bad! But I can fix this."
	wget -O - $BACKUPINSTALL | bash
fi

APP="PCFastManage"
DIRECTORY="/usr/local/bin/$APP"
BACKUPUPGRADE="https://gitlab.com/JonW/PCFastManage/raw/master/upgrade.sh"
UPGRADEFILE="$DIRECTORY/upgrade.sh"

if [ -d "$DIRECTORY" ]; then
	echo "$DIRECTORY exists"
	if [ -f $UPGRADEFILE ];then
		echo "$UPGRADEFILE exists"
		echo "Run Upgrade for $APP"
		bash $UPGRADEFILE $BRANCH
	else
		#There could be problems with other critical files such as the certificates or the config file but there is not much I can do about that.
		echo "the upgrade file for $APP was missing."
		echo "as a backup I will run from gitlab"
		wget -O - $BACKUPUPGRADE | bash -s $BRANCH
	fi
else
	echo "The directory does not exist."
	echo "The only way to fix this is to RMA it. As the certificates are gone!"
fi


