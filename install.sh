#!/bin/bash

DIRECTORY="/usr/local/bin/PCFastUpdater"

if [ -d "$DIRECTORY" ]; then
	echo "Upgrade PCFastUpdater the latest version"
	echo "Uninstall current version"
	rm -rf $DIRECTORY
	rm /lib/systemd/system/PCFastUpdater.service
	systemctl disable PCFastUpdater.service
	systemctl daemon-reload
	
	#git -C $DIRECTORY pull origin master
	#git -C $DIRECTORY submodule foreach git pull origin master
	#cp $DIRECTORY/PCFastUpdater.service /lib/systemd/system/PCFastUpdater.service
	#systemctl daemon-reload
fi

echo "Install PCFastUpdater"
mkdir $DIRECTORY
chmod 710 $DIRECTORY

git -C $DIRECTORY clone --recursive https://gitlab.com/JonW/PCFastUpdater.git .

cp $DIRECTORY/PCFastUpdater.service /lib/systemd/system/PCFastUpdater.service
cp $DIRECTORY/PCFastUpdater.timer /lib/systemd/system/PCFastUpdater.timer

systemctl daemon-reload
systemctl enable PCFastUpdater.timer
systemctl start PCFastUpdater.timer
