#!/bin/bash

DIRECTORY="/usr/local/bin/PCFastUpdater"

if [ -d "$DIRECTORY" ]; then
	echo "Upgrade PCFastUpdater the latest version"
	git -C $DIRECTORY fetch --all
	git -C $DIRECTORY reset --hard origin/master
	git -C $DIRECTORY submodule foreach git reset --hard
	git -C $DIRECTORY submodule update --recursive
	
else
	echo "Install PCFastUpdater"
	mkdir $DIRECTORY
	chmod 710 $DIRECTORY
	git -C $DIRECTORY clone --recursive https://gitlab.com/JonW/PCFastUpdater.git .

fi

cp $DIRECTORY/PCFastUpdater.service /lib/systemd/system/PCFastUpdater.service
cp $DIRECTORY/PCFastUpdater.timer /lib/systemd/system/PCFastUpdater.timer

systemctl daemon-reload
systemctl enable PCFastUpdater.timer

#Now that we are patched lets update
echo "Call Upgrade Script"
bash "$DIRECTORY/upgrade.sh"

echo "flush all write to disk and put disk into consistant state"
free && sync && echo 3 > /proc/sys/vm/drop_caches && free